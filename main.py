# needed to resolve home path
from os import path
# load in TUI class
from tui import TUI

# store database inside user's home directory
DATAPATH = path.expanduser("~/.todolist.db")

# declare a function to handle running the TUI
def main():
    # make a TUI object
    t = TUI(DATAPATH, "TODOLIST")
    # load the list of events
    t.run()
    # save and exit the database
    t.close()
# run program
main()