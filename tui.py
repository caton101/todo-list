# needed for special terminal characters and operations
from blessed import Terminal
# needed to read from event database
from fileEngine import FileEngine

# an array of months (used for sorting dates)
MONTHS = [
    "JAN",
    "FEB",
    "MAR",
    "APR",
    "MAY",
    "JUN",
    "JUL",
    "AUG",
    "SEP",
    "OCT",
    "NOV",
    "DEC"
]

# attach to terminal session
term = Terminal()

# declare a class used to make a TUI for managing events
class TUI(object):
    # make an instance of the TUI application
    def __init__(self, datapath, tablename):
        # open the event database
        self.fileEngine = FileEngine(datapath, tablename)

    # save and close the database
    def close(self):
        # safely close the database
        self.fileEngine.closeConnection()

    # get a 2d list of all events in the database
    def getEvents(self):
        # make container for events
        events = []
        # iterate over all events
        for e in self.fileEngine.getEvents():
            # elements must be added in this manner to convert a turple to a list
            events.append([e[0],e[1],e[2],e[3],e[4]])
        # sort the list with a month bias
        def rate(event):
            month = event[2][:3]
            done = event[3]
            score = 0
            if month in MONTHS:
                score += MONTHS.index(month) + 1
            if done == "YES":
                score += 13
            return score
        events.sort(key=rate)
        # return all events in database
        return events

    # get the character size of various columns in the database
    def getSizes(self):
        # calculate the max character size for the name column
        names = max(len(s) for s in self.fileEngine.getAllNameEntries())
        # calculate the max character size for the location column
        locations = max(len(s) for s in self.fileEngine.getAllLocationEntries())
        # calculate the max character size for the date column
        dates = max(len(s) for s in self.fileEngine.getAllDateEntries())
        # return sizes
        return names, locations, dates

    # toggle the done column of an event entry
    def toggleDone(self, index):
        # get the event out the database
        event = self.getEvents()[index]
        # get the future state of the done column
        state = ""
        # check if the event is currently marked as complete
        if event[3] == "YES":
            # change state to incomplete
            state = "NO"
        else:
            # change state to complete
            state = "YES"
        # save the new done column value using the event ID
        self.fileEngine.modifyDoneState(state, event[4])

    # made a new event and add it to the database
    def addEvent(self):
        # reset the terminal
        print(term.clear + term.home,end="")
        # get column values for event
        name = input("Enter name: ")
        location = input("Enter location: ")
        date = input("Enter date (MMM-DD-YYYY): ")
        # add new event to database using data that was just collected
        self.fileEngine.addEvent(name, location, date, "NO")

    # delete an event from the database
    def deleteEvent(self, index):
        # get event entry using given table index
        event = self.getEvents()[index]
        # remove event using its ID
        self.fileEngine.deleteEvent(event[4])

    # change the name of an event in the database
    def changeName(self, index):
        # get event entry using given table index
        event = self.getEvents()[index]
        # reset the terminal
        print(term.clear + term.home,end="")
        # get new name
        name = input("Enter new name: ")
        # modify event name using its ID
        self.fileEngine.modifyNameState(name, event[4])

    # change the location of an event in the database
    def changeLocation(self, index):
        # get event entry using given table index
        event = self.getEvents()[index]
        # reset the terminal
        print(term.clear + term.home,end="")
        # get new location
        location = input("Enter new location: ")
        # modify event location using its ID
        self.fileEngine.modifyLocationState(location, event[4])

    # change the date of an event in the database
    def changeDate(self, index):
        # get event entry using given table index
        event = self.getEvents()[index]
        # reset the terminal
        print(term.clear + term.home,end="")
        # get new date
        date = input("Enter new date (MMM-DD-YYYY): ")
        # modify event date using its ID
        self.fileEngine.modifyDateState(date, event[4])

    # this shows a list of options for modifying an event
    def showEditMenu(self, selectedIndex):
        # remember what list element is being processed
        index = 0;
        # reset terminal
        print(term.clear + term.home,end="")
        # iterate over menu options
        for option in["Change name", "Change location", "Change date", "Delete", "Cancel"]:
            # make container for terminal control characters
            color = ""
            # determine if current menu item is selected
            if selectedIndex == index:
                # invert terminal colors for selected item
                color = term.black_on_white
            else:
                # use normal terminal colors for unselected item
                color = term.white_on_black
            # write the menu item to the terminal
            print(color + option + term.white_on_black)
            # update the menu index
            index += 1

    # make a menu for editing an event
    def editEvent(self, index):
        # store currently selected menu item
        cursor = 0
        # enter into loop to process keystrokes
        while True:
            # show the list of options
            self.showEditMenu(cursor)
            # make container to store keypress
            key = None
            # get a keypress from the terminal
            with term.raw():
                key = term.inkey(timeout=None, esc_delay=0.1).code
            # process keystroke
            if key == term.KEY_DOWN:
                # move the cursor down if possible
                if cursor < 4:
                    cursor += 1;
            elif key == term.KEY_UP:
                # move the cursor up if possible
                if cursor > 0:
                    cursor -= 1
            elif key == term.KEY_ENTER:
                # modify event based on the selected menu item
                if cursor == 0:
                    # change event name
                    self.changeName(index)
                    break
                elif cursor == 1:
                    # change event location
                    self.changeLocation(index)
                    break
                elif cursor == 2:
                    # change event time
                    self.changeDate(index)
                    break
                elif cursor == 3:
                    # delete event
                    self.deleteEvent(index)
                    break
                elif cursor == 4:
                    # cancel editing
                    break
        
    # show a table with all events inside the database
    def showTable(self, selectedIndex, tablesize):
        print(term.clear + term.home,end="")
        # check if table has events
        if tablesize > 0:
            # get lengths of strings
            nameSize, locationSize, dateSize = self.getSizes()
            # print column labels
            print(term.white_on_black + "DONE  %s   %s   %s" % ("DATE".ljust(dateSize), "LOCATION".ljust(locationSize), "NAME".ljust(nameSize)))
            # make container to hold row position
            index = 0
            # show all events in database
            for event in self.getEvents():
                # make container to store terminal color
                color = ""
                # make container to store done column character
                done = ""
                # determine if event is selected
                if selectedIndex == index:
                    # use inverted colors for selected item
                    color = term.black_on_white
                else:
                    # use regular colors for regular item
                    color = term.white_on_black
                # determine if event is complete
                if event[3] == "YES":
                    # use X for a complete event
                    done = "X"
                else:
                    # use SPACE for an incomplete event
                    done = " "
                # print event in terminal
                # NOTE: all events must be adjusted for differences in text length
                print(color + "[%s]   %s   %s   %s" % (done,event[2].ljust(dateSize),event[1].ljust(locationSize),event[0].ljust(nameSize)) + term.white_on_black)
                # increment row
                index += 1
        # determine if the add task button is selected
        if selectedIndex == tablesize:
            # invert terminal colors for selected button
            print(term.black_on_white + "[ADD TASK]" + term.white_on_black)
        else:
            # use normal terminal colors for button
            print(term.white_on_black + "[ADD TASK]" + term.white_on_black)
        # reset terminal position
        print(term.home,end="")

    # render main menu and process input until closed
    def run(self):
        # track the selected event
        tableIndex = 0
        # show list of all events
        self.showTable(tableIndex, len(self.getEvents()))
        # process input until closed
        while True:
            # calculate the total number of events in the database
            tableSize = len(self.getEvents())
            # show list of all events
            self.showTable(tableIndex, tableSize)
            # make container to store keystroke
            key = None
            # get keystroke from terminal
            with term.raw():
                key = term.inkey(timeout=None, esc_delay=0.1).code
            # compare keystroke and perform requested action
            if key == term.KEY_DOWN:
                # if possible, move the selector down
                if tableIndex < tableSize:
                    tableIndex += 1;
            elif key == term.KEY_UP:
                # if possible, move the selector up
                if tableIndex > 0:
                    tableIndex -= 1
            elif key == term.KEY_ESCAPE:
                # close the main menu
                break
            elif key == term.KEY_DELETE:
                # delete an event
                self.deleteEvent(tableIndex)
            elif key == term.KEY_TAB:
                # toggle the complete status for selected event
                if tableIndex < tableSize:
                    self.toggleDone(tableIndex)
            elif key == term.KEY_ENTER:
                # determine if adding or modifying event
                if tableIndex < tableSize:
                    # modify selected event
                    self.editEvent(tableIndex)
                    continue
                else:
                    # add an event to the database
                    self.addEvent()
        # reset terminal on exit
        print(term.white_on_black + term.clear,end="")
