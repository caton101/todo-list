# needed for database io
import sqlite3
# needed to make ID tags
import time

# make class for database io
class FileEngine(object):
    # make object for database io
    def __init__(self, datapath, tablename):
        # save path to data and table name
        self.datapath = datapath
        self.tablename = tablename
        # open connection
        self.establishConnection()
        
    # initialize database
    def establishConnection(self):
        # open connection to database file
        self.connection = sqlite3.connect(self.datapath)
        # get a cursor
        self.cursor = self.connection.cursor()
        # create table if needed
        self.cursor.execute('create table if not exists %s (name text, location text, date text, done text, id integer)' % (self.tablename))
    
    # save all changes and close the database
    def closeConnection(self):
        # close the cursor
        self.cursor.close()
        # save any uncommitted changes
        self.connection.commit()
        # close the connection
        self.connection.close()
        
    # add a new event to the database
    def addEvent(self, name, location, date, done):
        # check if entry already exists
        self.cursor.execute("SELECT * FROM %s WHERE name=? AND location=? AND date=? AND done=?" % (self.tablename), (name, location, date, done))
        if len(self.cursor.fetchall()) == 0:
            # make a new entry in the table
            self.cursor.execute("INSERT INTO %s VALUES (?,?,?,?,?)" % (self.tablename), (name, location, date, done, time.time_ns()))
            # save changes
            self.connection.commit()
    
    # get a list of all events in the database
    def getEvents(self):
        # get list of events from table
        self.cursor.execute("SELECT * FROM %s ORDER BY date ASC" % (self.tablename))
        # return all results
        return self.cursor.fetchall()
    
    # get an event by searching for a name
    def getEventsByName(self, eventName):
        # get list of all events with specific name
        self.cursor.execute("SELECT * FROM %s WHERE name LIKE ?" % (self.tablename), ("%"+eventName+"%",))
        # return matches
        return self.cursor.fetchall()
    
    # get an event by searching for a location
    def getEventsByLocation(self, eventLocation):
        # get list of all events with specific name
        self.cursor.execute("SELECT * FROM %s WHERE location LIKE ?" % (self.tablename), ("%"+eventLocation+"%",))
        # return matches
        return self.cursor.fetchall()
    
    # get an event by searching for a date
    def getEventsByDate(self, eventDate):
        # get list of all events with specific name
        self.cursor.execute("SELECT * FROM %s WHERE date LIKE ?" % (self.tablename), ("%"+eventDate+"%",))
        # return matches
        return self.cursor.fetchall()
    
    # get an event by searching for a "done" state
    def getEventsByDone(self, eventDone):
        # get list of all events with specific name
        self.cursor.execute("SELECT * FROM %s WHERE done LIKE ?" % (self.tablename), ("%"+eventDone+"%",))
        # return matches
        return self.cursor.fetchall()
    
    # get a list of all event names in the database
    def getAllNameEntries(self):
        # get all row values for name
        self.cursor.execute("SELECT name FROM %s" % (self.tablename))
        # make container for names
        names = []
        # get all names in a list
        for n in self.cursor.fetchall():
            names.append(n[0])
        # return matches
        return names

    # get a list of all event locations in the database
    def getAllLocationEntries(self):
        # get all row values for locations
        self.cursor.execute("SELECT location FROM %s" % (self.tablename))
        # make container for locations
        locations = []
        # get all locations in a list
        for l in self.cursor.fetchall():
            locations.append(l[0])
        # return matches
        return locations

    # get a list of all event dates in the database
    def getAllDateEntries(self):
        # get all row values for dates
        self.cursor.execute("SELECT date FROM %s" % (self.tablename))
        # make container for dates
        dates = []
        # get all dates in a list
        for d in self.cursor.fetchall():
            dates.append(d[0])
        # return matches
        return dates
    
    # get a list of all event "done" states in the database
    def getAllDoneEntries(self):
        # get all row values for done
        self.cursor.execute("SELECT done FROM %s" % (self.tablename))
        # make container for dates
        dones = []
        # get all dones in a list
        for d in self.cursor.fetchall():
            dones.append(d[0])
        # return matches
        return dones
    
    # edit the "done" state of an event
    def modifyDoneState(self, doneState, eventID):
        # modify done state
        self.cursor.execute("UPDATE %s SET done=? WHERE id=?" % (self.tablename), (doneState, eventID))
        # save changes
        self.connection.commit()
    
    # edit the name of an event
    def modifyNameState(self, eventName, eventID):
        # modify name state
        self.cursor.execute("UPDATE %s SET name=? WHERE id=?" % (self.tablename), (eventName, eventID))
        # save changes
        self.connection.commit()
    
    # edit the location of an event
    def modifyLocationState(self, eventLocation, eventID):
        # modify location state
        self.cursor.execute("UPDATE %s SET location=? WHERE id=?" % (self.tablename), (eventLocation, eventID))
        # save changes
        self.connection.commit()
    
    # edit the date of an event
    def modifyDateState(self, eventDate, eventID):
        # modify date state
        self.cursor.execute("UPDATE %s SET date=? WHERE id=?" % (self.tablename), (eventDate, eventID))
        # save changes
        self.connection.commit()
    
    # delete an event
    def deleteEvent(self, eventID):
        # modify date state
        self.cursor.execute("DELETE FROM %s WHERE id=?" % (self.tablename), (eventID,))
        # save changes
        self.connection.commit()
